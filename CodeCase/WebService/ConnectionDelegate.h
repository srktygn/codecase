//
//  ConnectionDelegate.h
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ConnectionDelegateSuccess) (NSData *theData);
typedef void (^ConnectionDelegateFailure) (NSError *theError);

@interface ConnectionDelegate : NSObject <NSURLConnectionDelegate,NSURLConnectionDataDelegate>

@property (nonatomic,readwrite) NSTimeInterval timeout;
@property (nonatomic,readwrite) NSURLRequestCachePolicy cachePolicy;

- (void)fetchURL:(NSURL *)url withCompletion:(ConnectionDelegateSuccess)completion failure:(ConnectionDelegateFailure)failure;

- (void)cancelAllCalls;

@end
