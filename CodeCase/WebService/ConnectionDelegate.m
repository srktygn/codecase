//
//  ConnectionDelegate.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "ConnectionDelegate.h"

@interface BaseURLConnection : NSURLConnection

@property (nonatomic,readwrite,strong) ConnectionDelegateSuccess completion;
@property (nonatomic,readwrite,strong) ConnectionDelegateFailure failure;
@property (nonatomic,readwrite,strong) NSMutableData *data;
@property (nonatomic,readwrite,strong) NSURLResponse *response;

@end

@implementation BaseURLConnection

@end

@interface ConnectionDelegate ()

@property (nonatomic,readwrite,strong) NSMutableArray *connections;
@property (nonatomic,readwrite,strong) NSOperationQueue *networkQueue;

@end

@implementation ConnectionDelegate

- (id)init {
    self = [super init];
    if (self) {
        _networkQueue = [[NSOperationQueue alloc] init];
        // We just have 1 thread for this work, that way canceling is easy
        _networkQueue.maxConcurrentOperationCount = 1;
        _connections = [NSMutableArray arrayWithCapacity:10];
        _timeout = 5.0;
        _cachePolicy = NSURLRequestUseProtocolCachePolicy;
    }
    return self;
}

- (void)fetchURL:(NSURL *)url withCompletion:(ConnectionDelegateSuccess)completion failure:(ConnectionDelegateFailure)failure {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:self.cachePolicy timeoutInterval:self.timeout];
    BaseURLConnection *connection = [[BaseURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    if (!connection) {
        failure([NSError errorWithDomain:@"ConnectionDelegate" code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Could not initialize NSURLConnection"}]);
        return;
    }
    [connection setDelegateQueue:self.networkQueue];
    connection.data = [NSMutableData dataWithCapacity:1024];
    connection.completion = completion;
    connection.failure = failure;
    
    [connection start];
    
    [self.connections addObject:connection];
}

// This prevents new callbacks from being queued, cancels any queued callbacks
// from being run, and then adds an operation on the queue to cancel all
// connections and clean up the array we use.
- (void)cancelAllCalls {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.networkQueue setSuspended:YES];
    [self.networkQueue cancelAllOperations];
    [self.networkQueue addOperationWithBlock:^{
        for (BaseURLConnection *connection in self.connections) {
            [connection cancel];
            connection.failure([NSError errorWithDomain:@"ConnectionDelegate" code:-2 userInfo:@{NSLocalizedDescriptionKey: @"Call canceled by user"}]);
        }
        [self.connections removeAllObjects];
    }];
    [self.networkQueue setSuspended:NO];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    BaseURLConnection *exConnection = (BaseURLConnection *)connection;
    exConnection.failure(error);
    [self.connections removeObject:exConnection];
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    BaseURLConnection *exConnection = (BaseURLConnection *)connection;
    
    exConnection.response = response;
    
    exConnection.data.length = 0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    BaseURLConnection *exConnection = (BaseURLConnection *)connection;
    [exConnection.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.connections removeObject:connection];
    if (self.connections.count == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    BaseURLConnection *exConnection = (BaseURLConnection *)connection;
    
    if ([exConnection.response isKindOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)exConnection.response;
        if (httpResponse.statusCode >= 400) { // Client/Server Error
            exConnection.failure([NSError errorWithDomain:@"ConnectionDelegate" code:httpResponse.statusCode userInfo:@{NSLocalizedDescriptionKey: [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]}]);
            return;
        }
    }
    
    exConnection.completion(exConnection.data);
}

@end

