//
//  CC_BaseResult.h
//
//  Created by Serkut Yegin on 17.05.2016
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CC_BaseResult : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) double resultCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
