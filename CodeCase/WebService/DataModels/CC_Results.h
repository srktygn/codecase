//
//  CC_Results.h
//
//  Created by Serkut Yegin on 17.05.2016
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CC_Results : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *artworkUrl100;
@property (nonatomic, assign) double trackRentalPrice;
@property (nonatomic, strong) NSString *trackExplicitness;
@property (nonatomic, strong) NSString *collectionCensoredName;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) double collectionPrice;
@property (nonatomic, strong) NSArray *genres;
@property (nonatomic, strong) NSString *artworkUrl60;
@property (nonatomic, assign) double trackHdRentalPrice;
@property (nonatomic, strong) NSString *trackViewUrl;
@property (nonatomic, strong) NSString *artworkUrl30;
@property (nonatomic, strong) NSString *collectionName;
@property (nonatomic, strong) NSString *releaseDate;
@property (nonatomic, strong) NSString *feedUrl;
@property (nonatomic, strong) NSString *artistViewUrl;
@property (nonatomic, strong) NSString *wrapperType;
@property (nonatomic, assign) double collectionHdPrice;
@property (nonatomic, strong) NSArray *genreIds;
@property (nonatomic, strong) NSString *collectionExplicitness;
@property (nonatomic, assign) double trackId;
@property (nonatomic, strong) NSString *collectionViewUrl;
@property (nonatomic, strong) NSString *artworkUrl600;
@property (nonatomic, assign) double trackPrice;
@property (nonatomic, assign) double artistId;
@property (nonatomic, strong) NSString *artistName;
@property (nonatomic, assign) double trackHdPrice;
@property (nonatomic, assign) double collectionId;
@property (nonatomic, strong) NSString *trackCensoredName;
@property (nonatomic, strong) NSString *trackName;
@property (nonatomic, strong) NSString *contentAdvisoryRating;
@property (nonatomic, strong) NSString *kind;
@property (nonatomic, assign) double trackCount;
@property (nonatomic, strong) NSString *primaryGenreName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
