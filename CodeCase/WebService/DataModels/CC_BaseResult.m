//
//  CC_BaseResult.m
//
//  Created by Serkut Yegin on 17.05.2016
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "CC_BaseResult.h"
#import "CC_Results.h"


NSString *const kCC_BaseResultResults = @"results";
NSString *const kCC_BaseResultResultCount = @"resultCount";


@interface CC_BaseResult ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CC_BaseResult

@synthesize results = _results;
@synthesize resultCount = _resultCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedCC_Results = [dict objectForKey:kCC_BaseResultResults];
    NSMutableArray *parsedCC_Results = [NSMutableArray array];
    if ([receivedCC_Results isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedCC_Results) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                double trackId = [[item objectForKey:@"trackId"] doubleValue];
                if (![[DeleteManager shared] isItemDeletedWithTrackId:trackId]) {
                    [parsedCC_Results addObject:[CC_Results modelObjectWithDictionary:item]];
                }
            }
       }
    } else if ([receivedCC_Results isKindOfClass:[NSDictionary class]]) {
       [parsedCC_Results addObject:[CC_Results modelObjectWithDictionary:(NSDictionary *)receivedCC_Results]];
    }

    self.results = [NSArray arrayWithArray:parsedCC_Results];
            self.resultCount = [[self objectOrNilForKey:kCC_BaseResultResultCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForResults = [NSMutableArray array];
    for (NSObject *subArrayObject in self.results) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForResults addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForResults addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForResults] forKey:kCC_BaseResultResults];
    [mutableDict setValue:[NSNumber numberWithDouble:self.resultCount] forKey:kCC_BaseResultResultCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.results = [aDecoder decodeObjectForKey:kCC_BaseResultResults];
    self.resultCount = [aDecoder decodeDoubleForKey:kCC_BaseResultResultCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_results forKey:kCC_BaseResultResults];
    [aCoder encodeDouble:_resultCount forKey:kCC_BaseResultResultCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    CC_BaseResult *copy = [[CC_BaseResult alloc] init];
    
    if (copy) {

        copy.results = [self.results copyWithZone:zone];
        copy.resultCount = self.resultCount;
    }
    
    return copy;
}


@end
