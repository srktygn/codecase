//
//  CC_Results.m
//
//  Created by Serkut Yegin on 17.05.2016
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "CC_Results.h"


NSString *const kCC_ResultsArtworkUrl100 = @"artworkUrl100";
NSString *const kCC_ResultsTrackRentalPrice = @"trackRentalPrice";
NSString *const kCC_ResultsTrackExplicitness = @"trackExplicitness";
NSString *const kCC_ResultsCollectionCensoredName = @"collectionCensoredName";
NSString *const kCC_ResultsCountry = @"country";
NSString *const kCC_ResultsCurrency = @"currency";
NSString *const kCC_ResultsCollectionPrice = @"collectionPrice";
NSString *const kCC_ResultsGenres = @"genres";
NSString *const kCC_ResultsArtworkUrl60 = @"artworkUrl60";
NSString *const kCC_ResultsTrackHdRentalPrice = @"trackHdRentalPrice";
NSString *const kCC_ResultsTrackViewUrl = @"trackViewUrl";
NSString *const kCC_ResultsArtworkUrl30 = @"artworkUrl30";
NSString *const kCC_ResultsCollectionName = @"collectionName";
NSString *const kCC_ResultsReleaseDate = @"releaseDate";
NSString *const kCC_ResultsFeedUrl = @"feedUrl";
NSString *const kCC_ResultsArtistViewUrl = @"artistViewUrl";
NSString *const kCC_ResultsWrapperType = @"wrapperType";
NSString *const kCC_ResultsCollectionHdPrice = @"collectionHdPrice";
NSString *const kCC_ResultsGenreIds = @"genreIds";
NSString *const kCC_ResultsCollectionExplicitness = @"collectionExplicitness";
NSString *const kCC_ResultsTrackId = @"trackId";
NSString *const kCC_ResultsCollectionViewUrl = @"collectionViewUrl";
NSString *const kCC_ResultsArtworkUrl600 = @"artworkUrl600";
NSString *const kCC_ResultsTrackPrice = @"trackPrice";
NSString *const kCC_ResultsArtistId = @"artistId";
NSString *const kCC_ResultsArtistName = @"artistName";
NSString *const kCC_ResultsTrackHdPrice = @"trackHdPrice";
NSString *const kCC_ResultsCollectionId = @"collectionId";
NSString *const kCC_ResultsTrackCensoredName = @"trackCensoredName";
NSString *const kCC_ResultsTrackName = @"trackName";
NSString *const kCC_ResultsContentAdvisoryRating = @"contentAdvisoryRating";
NSString *const kCC_ResultsKind = @"kind";
NSString *const kCC_ResultsTrackCount = @"trackCount";
NSString *const kCC_ResultsPrimaryGenreName = @"primaryGenreName";


@interface CC_Results ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CC_Results

@synthesize artworkUrl100 = _artworkUrl100;
@synthesize trackRentalPrice = _trackRentalPrice;
@synthesize trackExplicitness = _trackExplicitness;
@synthesize collectionCensoredName = _collectionCensoredName;
@synthesize country = _country;
@synthesize currency = _currency;
@synthesize collectionPrice = _collectionPrice;
@synthesize genres = _genres;
@synthesize artworkUrl60 = _artworkUrl60;
@synthesize trackHdRentalPrice = _trackHdRentalPrice;
@synthesize trackViewUrl = _trackViewUrl;
@synthesize artworkUrl30 = _artworkUrl30;
@synthesize collectionName = _collectionName;
@synthesize releaseDate = _releaseDate;
@synthesize feedUrl = _feedUrl;
@synthesize artistViewUrl = _artistViewUrl;
@synthesize wrapperType = _wrapperType;
@synthesize collectionHdPrice = _collectionHdPrice;
@synthesize genreIds = _genreIds;
@synthesize collectionExplicitness = _collectionExplicitness;
@synthesize trackId = _trackId;
@synthesize collectionViewUrl = _collectionViewUrl;
@synthesize artworkUrl600 = _artworkUrl600;
@synthesize trackPrice = _trackPrice;
@synthesize artistId = _artistId;
@synthesize artistName = _artistName;
@synthesize trackHdPrice = _trackHdPrice;
@synthesize collectionId = _collectionId;
@synthesize trackCensoredName = _trackCensoredName;
@synthesize trackName = _trackName;
@synthesize contentAdvisoryRating = _contentAdvisoryRating;
@synthesize kind = _kind;
@synthesize trackCount = _trackCount;
@synthesize primaryGenreName = _primaryGenreName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.artworkUrl100 = [self objectOrNilForKey:kCC_ResultsArtworkUrl100 fromDictionary:dict];
            self.trackRentalPrice = [[self objectOrNilForKey:kCC_ResultsTrackRentalPrice fromDictionary:dict] doubleValue];
            self.trackExplicitness = [self objectOrNilForKey:kCC_ResultsTrackExplicitness fromDictionary:dict];
            self.collectionCensoredName = [self objectOrNilForKey:kCC_ResultsCollectionCensoredName fromDictionary:dict];
            self.country = [self objectOrNilForKey:kCC_ResultsCountry fromDictionary:dict];
            self.currency = [self objectOrNilForKey:kCC_ResultsCurrency fromDictionary:dict];
            self.collectionPrice = [[self objectOrNilForKey:kCC_ResultsCollectionPrice fromDictionary:dict] doubleValue];
            self.genres = [self objectOrNilForKey:kCC_ResultsGenres fromDictionary:dict];
            self.artworkUrl60 = [self objectOrNilForKey:kCC_ResultsArtworkUrl60 fromDictionary:dict];
            self.trackHdRentalPrice = [[self objectOrNilForKey:kCC_ResultsTrackHdRentalPrice fromDictionary:dict] doubleValue];
            self.trackViewUrl = [self objectOrNilForKey:kCC_ResultsTrackViewUrl fromDictionary:dict];
            self.artworkUrl30 = [self objectOrNilForKey:kCC_ResultsArtworkUrl30 fromDictionary:dict];
            self.collectionName = [self objectOrNilForKey:kCC_ResultsCollectionName fromDictionary:dict];
            self.releaseDate = [self objectOrNilForKey:kCC_ResultsReleaseDate fromDictionary:dict];
            self.feedUrl = [self objectOrNilForKey:kCC_ResultsFeedUrl fromDictionary:dict];
            self.artistViewUrl = [self objectOrNilForKey:kCC_ResultsArtistViewUrl fromDictionary:dict];
            self.wrapperType = [self objectOrNilForKey:kCC_ResultsWrapperType fromDictionary:dict];
            self.collectionHdPrice = [[self objectOrNilForKey:kCC_ResultsCollectionHdPrice fromDictionary:dict] doubleValue];
            self.genreIds = [self objectOrNilForKey:kCC_ResultsGenreIds fromDictionary:dict];
            self.collectionExplicitness = [self objectOrNilForKey:kCC_ResultsCollectionExplicitness fromDictionary:dict];
            self.trackId = [[self objectOrNilForKey:kCC_ResultsTrackId fromDictionary:dict] doubleValue];
            self.collectionViewUrl = [self objectOrNilForKey:kCC_ResultsCollectionViewUrl fromDictionary:dict];
            self.artworkUrl600 = [self objectOrNilForKey:kCC_ResultsArtworkUrl600 fromDictionary:dict];
            self.trackPrice = [[self objectOrNilForKey:kCC_ResultsTrackPrice fromDictionary:dict] doubleValue];
            self.artistId = [[self objectOrNilForKey:kCC_ResultsArtistId fromDictionary:dict] doubleValue];
            self.artistName = [self objectOrNilForKey:kCC_ResultsArtistName fromDictionary:dict];
            self.trackHdPrice = [[self objectOrNilForKey:kCC_ResultsTrackHdPrice fromDictionary:dict] doubleValue];
            self.collectionId = [[self objectOrNilForKey:kCC_ResultsCollectionId fromDictionary:dict] doubleValue];
            self.trackCensoredName = [self objectOrNilForKey:kCC_ResultsTrackCensoredName fromDictionary:dict];
            self.trackName = [self objectOrNilForKey:kCC_ResultsTrackName fromDictionary:dict];
            self.contentAdvisoryRating = [self objectOrNilForKey:kCC_ResultsContentAdvisoryRating fromDictionary:dict];
            self.kind = [self objectOrNilForKey:kCC_ResultsKind fromDictionary:dict];
            self.trackCount = [[self objectOrNilForKey:kCC_ResultsTrackCount fromDictionary:dict] doubleValue];
            self.primaryGenreName = [self objectOrNilForKey:kCC_ResultsPrimaryGenreName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.artworkUrl100 forKey:kCC_ResultsArtworkUrl100];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackRentalPrice] forKey:kCC_ResultsTrackRentalPrice];
    [mutableDict setValue:self.trackExplicitness forKey:kCC_ResultsTrackExplicitness];
    [mutableDict setValue:self.collectionCensoredName forKey:kCC_ResultsCollectionCensoredName];
    [mutableDict setValue:self.country forKey:kCC_ResultsCountry];
    [mutableDict setValue:self.currency forKey:kCC_ResultsCurrency];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectionPrice] forKey:kCC_ResultsCollectionPrice];
    NSMutableArray *tempArrayForGenres = [NSMutableArray array];
    for (NSObject *subArrayObject in self.genres) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForGenres addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForGenres addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForGenres] forKey:kCC_ResultsGenres];
    [mutableDict setValue:self.artworkUrl60 forKey:kCC_ResultsArtworkUrl60];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackHdRentalPrice] forKey:kCC_ResultsTrackHdRentalPrice];
    [mutableDict setValue:self.trackViewUrl forKey:kCC_ResultsTrackViewUrl];
    [mutableDict setValue:self.artworkUrl30 forKey:kCC_ResultsArtworkUrl30];
    [mutableDict setValue:self.collectionName forKey:kCC_ResultsCollectionName];
    [mutableDict setValue:self.releaseDate forKey:kCC_ResultsReleaseDate];
    [mutableDict setValue:self.feedUrl forKey:kCC_ResultsFeedUrl];
    [mutableDict setValue:self.artistViewUrl forKey:kCC_ResultsArtistViewUrl];
    [mutableDict setValue:self.wrapperType forKey:kCC_ResultsWrapperType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectionHdPrice] forKey:kCC_ResultsCollectionHdPrice];
    NSMutableArray *tempArrayForGenreIds = [NSMutableArray array];
    for (NSObject *subArrayObject in self.genreIds) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForGenreIds addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForGenreIds addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForGenreIds] forKey:kCC_ResultsGenreIds];
    [mutableDict setValue:self.collectionExplicitness forKey:kCC_ResultsCollectionExplicitness];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackId] forKey:kCC_ResultsTrackId];
    [mutableDict setValue:self.collectionViewUrl forKey:kCC_ResultsCollectionViewUrl];
    [mutableDict setValue:self.artworkUrl600 forKey:kCC_ResultsArtworkUrl600];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackPrice] forKey:kCC_ResultsTrackPrice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.artistId] forKey:kCC_ResultsArtistId];
    [mutableDict setValue:self.artistName forKey:kCC_ResultsArtistName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackHdPrice] forKey:kCC_ResultsTrackHdPrice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.collectionId] forKey:kCC_ResultsCollectionId];
    [mutableDict setValue:self.trackCensoredName forKey:kCC_ResultsTrackCensoredName];
    [mutableDict setValue:self.trackName forKey:kCC_ResultsTrackName];
    [mutableDict setValue:self.contentAdvisoryRating forKey:kCC_ResultsContentAdvisoryRating];
    [mutableDict setValue:self.kind forKey:kCC_ResultsKind];
    [mutableDict setValue:[NSNumber numberWithDouble:self.trackCount] forKey:kCC_ResultsTrackCount];
    [mutableDict setValue:self.primaryGenreName forKey:kCC_ResultsPrimaryGenreName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.artworkUrl100 = [aDecoder decodeObjectForKey:kCC_ResultsArtworkUrl100];
    self.trackRentalPrice = [aDecoder decodeDoubleForKey:kCC_ResultsTrackRentalPrice];
    self.trackExplicitness = [aDecoder decodeObjectForKey:kCC_ResultsTrackExplicitness];
    self.collectionCensoredName = [aDecoder decodeObjectForKey:kCC_ResultsCollectionCensoredName];
    self.country = [aDecoder decodeObjectForKey:kCC_ResultsCountry];
    self.currency = [aDecoder decodeObjectForKey:kCC_ResultsCurrency];
    self.collectionPrice = [aDecoder decodeDoubleForKey:kCC_ResultsCollectionPrice];
    self.genres = [aDecoder decodeObjectForKey:kCC_ResultsGenres];
    self.artworkUrl60 = [aDecoder decodeObjectForKey:kCC_ResultsArtworkUrl60];
    self.trackHdRentalPrice = [aDecoder decodeDoubleForKey:kCC_ResultsTrackHdRentalPrice];
    self.trackViewUrl = [aDecoder decodeObjectForKey:kCC_ResultsTrackViewUrl];
    self.artworkUrl30 = [aDecoder decodeObjectForKey:kCC_ResultsArtworkUrl30];
    self.collectionName = [aDecoder decodeObjectForKey:kCC_ResultsCollectionName];
    self.releaseDate = [aDecoder decodeObjectForKey:kCC_ResultsReleaseDate];
    self.feedUrl = [aDecoder decodeObjectForKey:kCC_ResultsFeedUrl];
    self.artistViewUrl = [aDecoder decodeObjectForKey:kCC_ResultsArtistViewUrl];
    self.wrapperType = [aDecoder decodeObjectForKey:kCC_ResultsWrapperType];
    self.collectionHdPrice = [aDecoder decodeDoubleForKey:kCC_ResultsCollectionHdPrice];
    self.genreIds = [aDecoder decodeObjectForKey:kCC_ResultsGenreIds];
    self.collectionExplicitness = [aDecoder decodeObjectForKey:kCC_ResultsCollectionExplicitness];
    self.trackId = [aDecoder decodeDoubleForKey:kCC_ResultsTrackId];
    self.collectionViewUrl = [aDecoder decodeObjectForKey:kCC_ResultsCollectionViewUrl];
    self.artworkUrl600 = [aDecoder decodeObjectForKey:kCC_ResultsArtworkUrl600];
    self.trackPrice = [aDecoder decodeDoubleForKey:kCC_ResultsTrackPrice];
    self.artistId = [aDecoder decodeDoubleForKey:kCC_ResultsArtistId];
    self.artistName = [aDecoder decodeObjectForKey:kCC_ResultsArtistName];
    self.trackHdPrice = [aDecoder decodeDoubleForKey:kCC_ResultsTrackHdPrice];
    self.collectionId = [aDecoder decodeDoubleForKey:kCC_ResultsCollectionId];
    self.trackCensoredName = [aDecoder decodeObjectForKey:kCC_ResultsTrackCensoredName];
    self.trackName = [aDecoder decodeObjectForKey:kCC_ResultsTrackName];
    self.contentAdvisoryRating = [aDecoder decodeObjectForKey:kCC_ResultsContentAdvisoryRating];
    self.kind = [aDecoder decodeObjectForKey:kCC_ResultsKind];
    self.trackCount = [aDecoder decodeDoubleForKey:kCC_ResultsTrackCount];
    self.primaryGenreName = [aDecoder decodeObjectForKey:kCC_ResultsPrimaryGenreName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_artworkUrl100 forKey:kCC_ResultsArtworkUrl100];
    [aCoder encodeDouble:_trackRentalPrice forKey:kCC_ResultsTrackRentalPrice];
    [aCoder encodeObject:_trackExplicitness forKey:kCC_ResultsTrackExplicitness];
    [aCoder encodeObject:_collectionCensoredName forKey:kCC_ResultsCollectionCensoredName];
    [aCoder encodeObject:_country forKey:kCC_ResultsCountry];
    [aCoder encodeObject:_currency forKey:kCC_ResultsCurrency];
    [aCoder encodeDouble:_collectionPrice forKey:kCC_ResultsCollectionPrice];
    [aCoder encodeObject:_genres forKey:kCC_ResultsGenres];
    [aCoder encodeObject:_artworkUrl60 forKey:kCC_ResultsArtworkUrl60];
    [aCoder encodeDouble:_trackHdRentalPrice forKey:kCC_ResultsTrackHdRentalPrice];
    [aCoder encodeObject:_trackViewUrl forKey:kCC_ResultsTrackViewUrl];
    [aCoder encodeObject:_artworkUrl30 forKey:kCC_ResultsArtworkUrl30];
    [aCoder encodeObject:_collectionName forKey:kCC_ResultsCollectionName];
    [aCoder encodeObject:_releaseDate forKey:kCC_ResultsReleaseDate];
    [aCoder encodeObject:_feedUrl forKey:kCC_ResultsFeedUrl];
    [aCoder encodeObject:_artistViewUrl forKey:kCC_ResultsArtistViewUrl];
    [aCoder encodeObject:_wrapperType forKey:kCC_ResultsWrapperType];
    [aCoder encodeDouble:_collectionHdPrice forKey:kCC_ResultsCollectionHdPrice];
    [aCoder encodeObject:_genreIds forKey:kCC_ResultsGenreIds];
    [aCoder encodeObject:_collectionExplicitness forKey:kCC_ResultsCollectionExplicitness];
    [aCoder encodeDouble:_trackId forKey:kCC_ResultsTrackId];
    [aCoder encodeObject:_collectionViewUrl forKey:kCC_ResultsCollectionViewUrl];
    [aCoder encodeObject:_artworkUrl600 forKey:kCC_ResultsArtworkUrl600];
    [aCoder encodeDouble:_trackPrice forKey:kCC_ResultsTrackPrice];
    [aCoder encodeDouble:_artistId forKey:kCC_ResultsArtistId];
    [aCoder encodeObject:_artistName forKey:kCC_ResultsArtistName];
    [aCoder encodeDouble:_trackHdPrice forKey:kCC_ResultsTrackHdPrice];
    [aCoder encodeDouble:_collectionId forKey:kCC_ResultsCollectionId];
    [aCoder encodeObject:_trackCensoredName forKey:kCC_ResultsTrackCensoredName];
    [aCoder encodeObject:_trackName forKey:kCC_ResultsTrackName];
    [aCoder encodeObject:_contentAdvisoryRating forKey:kCC_ResultsContentAdvisoryRating];
    [aCoder encodeObject:_kind forKey:kCC_ResultsKind];
    [aCoder encodeDouble:_trackCount forKey:kCC_ResultsTrackCount];
    [aCoder encodeObject:_primaryGenreName forKey:kCC_ResultsPrimaryGenreName];
}

- (id)copyWithZone:(NSZone *)zone
{
    CC_Results *copy = [[CC_Results alloc] init];
    
    if (copy) {

        copy.artworkUrl100 = [self.artworkUrl100 copyWithZone:zone];
        copy.trackRentalPrice = self.trackRentalPrice;
        copy.trackExplicitness = [self.trackExplicitness copyWithZone:zone];
        copy.collectionCensoredName = [self.collectionCensoredName copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.currency = [self.currency copyWithZone:zone];
        copy.collectionPrice = self.collectionPrice;
        copy.genres = [self.genres copyWithZone:zone];
        copy.artworkUrl60 = [self.artworkUrl60 copyWithZone:zone];
        copy.trackHdRentalPrice = self.trackHdRentalPrice;
        copy.trackViewUrl = [self.trackViewUrl copyWithZone:zone];
        copy.artworkUrl30 = [self.artworkUrl30 copyWithZone:zone];
        copy.collectionName = [self.collectionName copyWithZone:zone];
        copy.releaseDate = [self.releaseDate copyWithZone:zone];
        copy.feedUrl = [self.feedUrl copyWithZone:zone];
        copy.artistViewUrl = [self.artistViewUrl copyWithZone:zone];
        copy.wrapperType = [self.wrapperType copyWithZone:zone];
        copy.collectionHdPrice = self.collectionHdPrice;
        copy.genreIds = [self.genreIds copyWithZone:zone];
        copy.collectionExplicitness = [self.collectionExplicitness copyWithZone:zone];
        copy.trackId = self.trackId;
        copy.collectionViewUrl = [self.collectionViewUrl copyWithZone:zone];
        copy.artworkUrl600 = [self.artworkUrl600 copyWithZone:zone];
        copy.trackPrice = self.trackPrice;
        copy.artistId = self.artistId;
        copy.artistName = [self.artistName copyWithZone:zone];
        copy.trackHdPrice = self.trackHdPrice;
        copy.collectionId = self.collectionId;
        copy.trackCensoredName = [self.trackCensoredName copyWithZone:zone];
        copy.trackName = [self.trackName copyWithZone:zone];
        copy.contentAdvisoryRating = [self.contentAdvisoryRating copyWithZone:zone];
        copy.kind = [self.kind copyWithZone:zone];
        copy.trackCount = self.trackCount;
        copy.primaryGenreName = [self.primaryGenreName copyWithZone:zone];
    }
    
    return copy;
}


@end
