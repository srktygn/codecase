//
//  DetailTableCell.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "DetailTableCell.h"

@interface DetailTableCell ()
@property (strong, nonatomic) IBOutlet UILabel *lblKey;
@property (strong, nonatomic) IBOutlet UILabel *lblValue;

@end

@implementation DetailTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setupUIWithKey:(NSString*)key value:(NSString*)value
{
    _lblKey.text = key;
    _lblValue.text = value;
}

@end
