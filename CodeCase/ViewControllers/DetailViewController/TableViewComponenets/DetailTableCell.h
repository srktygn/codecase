//
//  DetailTableCell.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableCell : UITableViewCell

-(void)setupUIWithKey:(NSString*)key value:(NSString*)value;

@end
