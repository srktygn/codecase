//
//  DetailViewController.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "BaseViewController.h"

@protocol DetailViewControllerDelegate <NSObject>

-(void)selectedItemDeleted;

@end

@interface DetailViewController : BaseViewController

#pragma mark - Delegate
@property (nonatomic,weak) id <DetailViewControllerDelegate> delegate;

#pragma mark - Public Props
@property (nonatomic,strong) CC_Results *selectedItem;

@end
