//
//  DetailViewController.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailTableCell.h"

#define SIZE_IMAGE_DEFAULT CGSizeMake(300,300)
#define TABLE_ROW_HEIGHT 44

@interface DetailViewController () <UITabBarDelegate,UITableViewDataSource,UIAlertViewDelegate>

#pragma mark - IBOutlets

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTableHeight;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraingScrollContentWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintScrollContentHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintImageHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintImageWidth;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewScrollContent;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

#pragma mark - Props

@property (nonatomic,strong) NSMutableDictionary *dictDetails;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self generateItemDictionary];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupUI];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self handleScrollContent];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupUI
{
    _constraintTableHeight.constant = _dictDetails.allKeys.count * TABLE_ROW_HEIGHT;
    [_tableView layoutIfNeeded];
    
    NSString *imageUrlToShow = [self decideWhichImageUrlToShow];
    
    if (self.selectedItem && !imageUrlToShow.isEmpty) {
        
        __weak DetailViewController *weakSelf = self;
        
        [[[UtilityFunctions alloc] init] getCachedImage:imageUrlToShow
                                          cacheFileName:[NSString stringWithFormat:@"bigger%d",(int)self.selectedItem.trackId]
                                             completion:^(UIImage *cachedImage) {
                                                 
                                                 [weakSelf setDownloadedImage:cachedImage];
        }];
    }
}

-(void)setDownloadedImage:(UIImage*)image
{
    CGSize size = image.size;
    if (image.size.width > _viewScrollContent.frame.size.width || image.size.height > _viewScrollContent.frame.size.height) {
        size = SIZE_IMAGE_DEFAULT;
    }
    _constraintImageWidth.constant = size.width;
    _constraintImageHeight.constant = size.height;
    [_imageView layoutIfNeeded];
    [_tableView layoutIfNeeded];
    _imageView.image = image;
    
    [self handleScrollContent];
}

-(NSString*)decideWhichImageUrlToShow
{
    if (self.selectedItem) {
        if (self.selectedItem.artworkUrl600 && !self.selectedItem.artworkUrl600.isEmpty) {
            return self.selectedItem.artworkUrl600;
        }
        else if (self.selectedItem.artworkUrl100 && !self.selectedItem.artworkUrl100.isEmpty) {
            return self.selectedItem.artworkUrl100;
        }
        else if (self.selectedItem.artworkUrl60 && !self.selectedItem.artworkUrl60.isEmpty) {
            return self.selectedItem.artworkUrl60;
        }
        else if (self.selectedItem.artworkUrl30 && !self.selectedItem.artworkUrl30.isEmpty) {
            return self.selectedItem.artworkUrl30;
        }
        else {
            return @"";
        }
    } else {
        return @"";
    }
}

-(void)handleScrollContent
{
    if (_tableView.frame.size.height + _tableView.frame.origin.y > self.view.frame.size.height) {
        _constraintScrollContentHeight.constant = _tableView.frame.size.height + _tableView.frame.origin.y + 10;
    } else {
        _constraintScrollContentHeight.constant = self.view.frame.size.height;
    }
    
    _constraingScrollContentWidth.constant = self.view.frame.size.width;
    [_viewScrollContent layoutIfNeeded];
    _scrollView.contentSize = _viewScrollContent.frame.size;
}

#pragma mark - UITableView Delegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dictDetails.allKeys.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailTableCell class])];
    
    if(cell==nil)
        cell = [[DetailTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([DetailTableCell class])];
    
    NSString *stringKey = _dictDetails.allKeys[indexPath.row];
    [cell setupUIWithKey:stringKey value:[_dictDetails valueForKey:stringKey]];
    return cell;
}

#pragma mark - IBActions

-(IBAction)barButtonDeleteTapped:(UIBarButtonItem*)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"If you delete this item, you will never show it again. Do you confirm?" delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Confirm", nil];
    [alertView show];
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if ([self.delegate respondsToSelector:@selector(selectedItemDeleted)]) {
            [self.delegate selectedItemDeleted];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Utils

-(void)generateItemDictionary
{
    _dictDetails = [NSMutableDictionary dictionary];
    
    if (_selectedItem.artistName && !_selectedItem.artistName.isEmpty) {
        [_dictDetails setObject:_selectedItem.artistName forKey:@"Artist Name"];
    }
    if (_selectedItem.trackName && !_selectedItem.trackName.isEmpty) {
        [_dictDetails setObject:_selectedItem.trackName forKey:@"Track Name"];
    }
    if (_selectedItem.collectionName && !_selectedItem.collectionName.isEmpty) {
        [_dictDetails setObject:_selectedItem.collectionName forKey:@"Collection Name"];
    }
    if (_selectedItem.primaryGenreName && !_selectedItem.primaryGenreName.isEmpty) {
        [_dictDetails setObject:_selectedItem.primaryGenreName forKey:@"Genre Name"];
    }
    if (_selectedItem.releaseDate && !_selectedItem.releaseDate.isEmpty) {
        NSDate *releaseDate = [NSDate dateFromString:_selectedItem.releaseDate];
        [_dictDetails setObject:[NSDate dateStringWithDate:releaseDate] forKey:@"Release Date"];
    }
    [_dictDetails setObject:[NSString stringWithFormat:@"%.2f %@",_selectedItem.trackRentalPrice,_selectedItem.currency] forKey:@"Track Rental Price"];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
