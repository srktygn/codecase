//
//  BaseViewController.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "BaseViewController.h"
#import "ConnectionDelegate.h"

@interface BaseViewController ()

#pragma mark - Props
@property (nonatomic,strong) ConnectionDelegate *connection;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor appMainColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utils

-(void)showAlertViewWithTitle:(NSString*)title message:(NSString*)message
{
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Web Service

-(void)callServiceWithUrlString:(NSString *)urlString success:(void (^)(NSDictionary *))success fail:(void (^)(NSUInteger))fail
{
    if (!self.connection) {
        self.connection = [ConnectionDelegate new];
    }
    
    [self.connection fetchURL:[NSURL URLWithString:urlString] withCompletion:^(NSData *theData) {
        
        NSError *error;
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:theData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
        
        if (!error && jsonData) {
            
            if (success) {
                success(jsonData);
            }
            
        } else {
            if (fail) {
                fail(ERROR_TYPE_PARSE);
            }
        }
        
    } failure:^(NSError *theError) {
        if (fail) {
            fail(ERROR_TYPE_SERVICE);
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
