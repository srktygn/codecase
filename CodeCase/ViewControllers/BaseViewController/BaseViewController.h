//
//  BaseViewController.h
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,ERROR_TYPE){
    ERROR_TYPE_SERVICE = 100,
    ERROR_TYPE_PARSE = 200
};

@interface BaseViewController : UIViewController

/**
 *  Shows a basic alert view with a title and a message.
 */
-(void)showAlertViewWithTitle:(NSString*)title message:(NSString*)message;

/**
 *  Makes service call.
 *
 *  @param urlString string of the web service request
 *  @param success success block of the request. returns the parsed data.
 *  @param fail error block of the request. returns the error type.
 *
 */
-(void)callServiceWithUrlString:(NSString*)urlString
                        success:(void(^)(NSDictionary *data))success
                           fail:(void(^)(NSUInteger errorType))fail;
@end
