//
//  ResultCollectionViewCell.m
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "ResultCollectionViewCell.h"
@import ImageIO;

@interface ResultCollectionViewCell ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ResultCollectionViewCell

- (id)init {
    
    self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil].firstObject;
    
    if (self) {}
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.titleLabel.text = @"";
    self.imageView.image = nil;
}

-(void)setupUIWithResult:(CC_Results *)result isVisitedBefore:(BOOL)isVisitedBefore
{
    self.titleLabel.text = [NSString stringWithFormat:@"%@ - %@",result.artistName,result.trackName];
    self.titleLabel.textColor = isVisitedBefore ? [UIColor lightGrayColor] : [UIColor blackColor];
    
    [[[UtilityFunctions alloc] init] getCachedImage:result.artworkUrl30 cacheFileName:[NSString stringWithFormat:@"thumbnail%d",(int)result.trackId] completion:^(UIImage *cachedImage) {
        self.imageView.image = cachedImage;
    }];
}

@end
