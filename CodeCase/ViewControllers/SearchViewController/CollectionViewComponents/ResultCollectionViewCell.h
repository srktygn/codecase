//
//  ResultCollectionViewCell.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultCollectionViewCell : UICollectionViewCell

-(void)setupUIWithResult:(CC_Results *)result isVisitedBefore:(BOOL)isVisitedBefore;

@end
