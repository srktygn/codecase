//
//  SearchViewController.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchHelper.h"
#import "ResultCollectionViewCell.h"
#import "MediaSelectionViewController.h"
#import "DetailViewController.h"

#define DEFAULT_ROW_ITEM_SIZE 1
#define MULTIPLE_ROW_ITEM_SIZE 2

#define SEARCH_BAR_CONSTANT_CLOSED -44.0f
#define SEARCH_BAR_CONSTANT_OPENED 0

typedef NS_ENUM(NSInteger,RowType)
{
    RowTypeDefault,
    RowTypeMultiple
};

@interface SearchViewController () <UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,PopupManagerDelegate,MediaSelectionViewControllerDelegate,DetailViewControllerDelegate,UISearchBarDelegate> {
    CGSize sizeUpdated;
    BOOL detailVCShown,selectedItemDeleted,isFiltered;
    NSInteger selectedItemIndex;
}

#pragma mark - IBOutlets
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSearchBarTop;

#pragma mark - Props
@property (nonatomic) SearchViewType searchViewType;
@property (nonatomic) RowType rowType;
@property (nonatomic,strong) CC_BaseResult *searchResults;
@property (nonatomic) BOOL searchBarClosed;
@property (nonatomic,strong) NSArray *arrFilteredResults;

@end

@implementation SearchViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self handleRowTypeForCurrentOrientation];
    [self callService];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (detailVCShown) {
        [self handleRowTypeForCurrentOrientation];
        [self.collectionView reloadData];
        detailVCShown = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (selectedItemDeleted) {
        [self deleteItemWithAnimation];
        selectedItemDeleted = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view findAndResignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews;
{
    [super viewWillLayoutSubviews];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        NSInteger index = indexPath.row;
        
        [[VisitManager shared] itemVisited:[self decideCollectionDataSource][index]];
        DetailViewController *controller = (DetailViewController *)[segue destinationViewController];
        controller.delegate = self;
        controller.selectedItem = [self decideCollectionDataSource][index];
        
        selectedItemIndex = index;
        detailVCShown = YES;
    }
}


#pragma mark - IBActions

- (IBAction)barButtonSelectMediaTapped:(UIBarButtonItem *)sender
{
    MediaSelectionViewController *mediaSelectionVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MediaSelectionViewController class])];
    mediaSelectionVC.selectedSearchType = _searchViewType;
    mediaSelectionVC.delegate = self;
    [[PopupManager shared] showDialog:mediaSelectionVC delegate:self];
}

#pragma mark - Collection View Datasources & Delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = 0;
    NSArray *arrItems = [self decideCollectionDataSource];
    if (arrItems) {
        items = arrItems.count;
    }
    
    return items;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ResultCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ResultCollectionViewCell class]) forIndexPath:indexPath];
    
    CC_Results *result = [self decideCollectionDataSource][indexPath.row];
    
    [cell setupUIWithResult:result isVisitedBefore:[[VisitManager shared] isItemVisitedBefore:result]];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(_rowType == RowTypeDefault ? collectionView.frame.size.width : collectionView.frame.size.width/MULTIPLE_ROW_ITEM_SIZE, 50);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > 0) {
        self.searchBarClosed = YES;
    } else if(scrollView.contentOffset.y < -10) {
        self.searchBarClosed = NO;
    }
}

#pragma mark - Web Service

-(void)callService {
    
    [self callServiceWithUrlString:[SearchHelper createSearchUrlStringWithSearchType:self.searchViewType]
                           success:^(NSDictionary *data) {
                               [self responseReceived:data];
                           } fail:^(NSUInteger errorType) {
                               [self showAlertViewWithTitle:@"Error" message:@"No item found."];
    }];
}

-(void)responseReceived:(NSDictionary*)response
{
    _searchResults = [[CC_BaseResult alloc] initWithDictionary:response];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (isFiltered) {
            [self filterResultsForText:_searchBar.text];
        }
        [self refreshCollection];
        });
}

#pragma mark - SearchBar Handling

-(void)setSearchBarClosed:(BOOL)searchBarClosed
{
    if (_searchBarClosed != searchBarClosed) {
        _searchBarClosed = searchBarClosed;
        
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:500 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.constraintSearchBarTop.constant = _searchBarClosed ? SEARCH_BAR_CONSTANT_CLOSED : SEARCH_BAR_CONSTANT_OPENED;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        isFiltered = NO;
    }
    else
    {
        isFiltered = YES;
        [self filterResultsForText:searchText];
    }
    
    [self refreshCollection];
}

-(void)filterResultsForText:(NSString*)searchText
{
    _arrFilteredResults = [NSArray array];
    _arrFilteredResults = [_searchResults.results filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        CC_Results *object = evaluatedObject;
        return [object.artistName hasPrefix:searchText] || [object.trackName hasPrefix:searchText]  || [object.collectionName hasPrefix:searchText] || [object.primaryGenreName hasPrefix:searchText];
    }]];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view findAndResignFirstResponder];
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (![text isAlphaNumerical]) {
        return NO;
    }
    NSUInteger newLength = [searchBar.text length] + [text length] - range.length;
    return (newLength <= 10);
}

#pragma mark - Orientation

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self handleRowTypeForOrientation:toInterfaceOrientation];
    [self.collectionView reloadData];
}

-(void)handleRowTypeForOrientation:(UIInterfaceOrientation)orientation
{
    if (IPAD) {
        _rowType = RowTypeMultiple;
    } else {
        
        if (orientation ==  UIInterfaceOrientationPortrait)
        {
            _rowType = RowTypeDefault;
        }
        else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft)
        {
            _rowType = RowTypeMultiple;
        }
    }
}

-(void)handleRowTypeForCurrentOrientation
{
    [self handleRowTypeForOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

#pragma mark - PopupManager Delegate

-(void)mediaSelectionVC:(MediaSelectionViewController *)mediaSelectionVC didChangeSelectionWithType:(SearchViewType)newType
{
    _searchViewType = newType;
    [self callService];
}

#pragma mark - DetailVC Delegate & Delete Animation

-(void)selectedItemDeleted
{
    selectedItemDeleted = YES;
    CC_Results *deletedResult = [[self decideCollectionDataSource] objectAtIndex:selectedItemIndex];
    [[DeleteManager shared] itemDeleted:deletedResult];
}

-(void)deleteItemWithAnimation
{
    [self.collectionView performBatchUpdates:^{
        NSMutableArray *arrTemp = [NSMutableArray arrayWithArray:[self decideCollectionDataSource]];
        CC_Results *deletedResult = [arrTemp objectAtIndex:selectedItemIndex];
        [arrTemp removeObjectAtIndex:selectedItemIndex];
        
        if (isFiltered) {
            _arrFilteredResults = [NSArray arrayWithArray:arrTemp];
            
            //for consistency between the filtered array and main array
            NSMutableArray *arrTempForMainDataSource = [NSMutableArray arrayWithArray:_searchResults.results];
            [arrTempForMainDataSource removeObject:deletedResult];
            _searchResults.results = [NSArray arrayWithArray:arrTempForMainDataSource];
            
        } else
            _searchResults.results = [NSArray arrayWithArray:arrTemp];
        
        [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:selectedItemIndex inSection:0]]];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Utils

-(void)refreshCollection
{
    [self.collectionView reloadData];
    self.searchBarClosed = NO;
    if ([self decideCollectionDataSource] && [self decideCollectionDataSource].count>0) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
}

-(NSArray*)decideCollectionDataSource
{
    if (isFiltered) {
        return _arrFilteredResults;
    } else {
        return _searchResults.results;
    }
}

@end
