//
//  main.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
