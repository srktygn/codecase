//
//  SearchHelper.h
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger,SearchViewType) {
    SearchViewTypeAll = 0,
    SearchViewTypeMovie,
    SearchViewTypePodcast,
    SearchViewTypeMusic,
};

#define SEARCH_BASE_URL @"https://itunes.apple.com/search?"
#define SEARCH_LIMIT 100
#define SEARCH_DEFAULT_TERM @"jack"

@interface SearchHelper : NSObject

+(NSString*)getMediaStringAccordingToSearchType:(SearchViewType)searchType;
+(NSString*)createSearchUrlStringWithSearchType:(SearchViewType)searchType;

@end
