//
//  SearchHelper.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "SearchHelper.h"

#define MEDIA_ALL @"all";
#define MEDIA_MOVIE @"movie";
#define MEDIA_PODCAST @"podcast";
#define MEDIA_MUSIC @"music";

@implementation SearchHelper

+(NSString*)getMediaStringAccordingToSearchType:(SearchViewType)searchType
{
    NSString *stringTerm = @"";
    
    switch (searchType) {
        case SearchViewTypeAll:
            stringTerm = MEDIA_ALL;
            break;
        case SearchViewTypeMovie:
            stringTerm = MEDIA_MOVIE;
            break;
        case SearchViewTypePodcast:
            stringTerm = MEDIA_PODCAST;
            break;
        case SearchViewTypeMusic:
            stringTerm = MEDIA_MUSIC;
            break;
            
        default:
            break;
    }
    
    return stringTerm;
}

+(NSString*)createSearchUrlStringWithSearchType:(SearchViewType)searchType
{
    NSString *searchString = SEARCH_BASE_URL;
    
    //limited to 100 items...
    searchString = [searchString stringByAppendingString:[NSString stringWithFormat:@"limit=%d",SEARCH_LIMIT]];
    //sets the media type as all,movie,podcast,music...
    searchString = [searchString stringByAppendingString:[NSString stringWithFormat:@"&media=%@",[SearchHelper getMediaStringAccordingToSearchType:searchType]]];
    
    searchString = [searchString stringByAppendingString:[NSString stringWithFormat:@"&term=%@",SEARCH_DEFAULT_TERM]];
    
    return searchString;
}

@end
