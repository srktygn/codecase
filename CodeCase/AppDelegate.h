//
//  AppDelegate.h
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

