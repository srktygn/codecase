//
//  VisitManager.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//


@end
#import <Foundation/Foundation.h>

@interface VisitManager : NSObject

#pragma mark - Public Methods
+ (id)shared;

-(BOOL)isItemVisitedBefore:(CC_Results*)result;
-(void)itemVisited:(CC_Results*)item;

@end
