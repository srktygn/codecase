//
//  VisitManager.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "VisitManager.h"

#define USER_DEFAULTS_KEY_VISIT @"visitedItems"

static id visitManager = nil;

@interface VisitManager ()

@property (nonatomic,strong) NSMutableArray *arrVisitedItems;
@end

@implementation VisitManager

+(id)shared
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        visitManager = [[super allocWithZone:NULL] init];
    });
    
    return visitManager;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return [self shared];
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _arrVisitedItems = [NSMutableArray new];
        [self readItemsFromUserDefaults];
    }
    
    return self;
}

-(BOOL)isItemVisitedBefore:(CC_Results*)result
{
    __block BOOL isVisited = NO;
    [_arrVisitedItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSNumber *visitedResult = obj;
        if (visitedResult.doubleValue == result.trackId) {
            isVisited = YES;
            *stop = YES;
        }
    }];
    
    return isVisited;
}

-(void)itemVisited:(CC_Results*)item
{
    if (![self isItemVisitedBefore:item]) {
        [_arrVisitedItems addObject:[NSNumber numberWithDouble:item.trackId]];
        [self writeItemsToUserDefaults];
    }
}

-(void)readItemsFromUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrStoredItems = [defaults objectForKey:USER_DEFAULTS_KEY_VISIT];
    
    if (arrStoredItems) {
        _arrVisitedItems = [NSMutableArray arrayWithArray:arrStoredItems];
    }
}

-(void)writeItemsToUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_arrVisitedItems forKey:USER_DEFAULTS_KEY_VISIT];
    [defaults synchronize];
}

@end
