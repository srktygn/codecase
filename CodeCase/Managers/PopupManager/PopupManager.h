//
//  PopupManager.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PopupContainerView;

#define DEFAULT_POPUP_SIZE CGSizeMake(280,290)

@protocol PopupManagerDelegate <NSObject>

@optional
-(void)popupDidPresented:(PopupContainerView*)popupContainerView;
-(void)popupDidDismiss:(PopupContainerView*)popupContainerView;
-(BOOL)shouldShowTitle;

@end

@interface PopupManager : NSObject

#pragma mark - Delegate
@property (nonatomic,weak) id<PopupManagerDelegate> delegate;

#pragma mark - Properties
@property (nonatomic,strong,readonly) PopupContainerView *activePopup;
@property (nonatomic,strong,readonly) UIViewController *presentedVC;

#pragma mark - Public Methods
+ (id)shared;

- (void)showDialog:(UIViewController*)dialog delegate:(id<PopupManagerDelegate>)delegate;
- (void)showDialog:(UIViewController*)dialog forSize:(CGSize)size delegate:(id<PopupManagerDelegate>)delegate;
- (void)closeDialog;

