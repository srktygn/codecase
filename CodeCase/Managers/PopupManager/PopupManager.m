//
//  PopupManager.m
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "PopupManager.h"
#import "PopupContainerView.h"

static id dialogManager = nil;

@implementation PopupManager

+(id)shared
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dialogManager = [[super allocWithZone:NULL] init];
    });
    
    return dialogManager;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return [self shared];
}

- (id)init
{
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

#pragma mark - Show&Hide Methods

- (void)showDialog:(UIViewController*)dialog delegate:(id<PopupManagerDelegate>)delegate
{
    [self showDialog:dialog forSize:DEFAULT_POPUP_SIZE delegate:delegate];
}

- (void)showDialog:(UIViewController*)dialog forSize:(CGSize)size delegate:(id<PopupManagerDelegate>)delegate
{
    if (_activePopup && _presentedVC) {
        return;
    }
    
    _presentedVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    self.delegate = delegate;
    
    BOOL showTitle = YES;
    if ([_delegate respondsToSelector:@selector(shouldShowTitle)]) {
        showTitle = [_delegate shouldShowTitle];
    }
    
    _activePopup = [PopupContainerView dialogWithController:dialog size:size showTitle:showTitle];
    
    [_presentedVC.view addSubview:_activePopup];
    
    [_activePopup setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_activePopup.superview addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-0-[_activePopup]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(_activePopup)]];
    [_activePopup.superview addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-0-[_activePopup]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(_activePopup)]];
    
    
    [_activePopup layoutIfNeeded];
    [_presentedVC.view layoutIfNeeded];
    
    if ([_delegate respondsToSelector:@selector(popupDidPresented:)]) {
        [_delegate popupDidPresented:_activePopup];
    }
}

- (void)closeDialog
{
    [_activePopup closePopupWithCompletion:^{
        if ([_delegate respondsToSelector:@selector(popupDidDismiss:)]) {
            [_delegate popupDidDismiss:_activePopup];
        }
        _activePopup = nil;
        _presentedVC = nil;
        _delegate = nil;
    }];
}

@end
