//
//  PopupContainerView.h
//  Halkbank
//
//  Created by Serkut YEGİN on 16.09.2015.
//  Copyright © 2015 tmobtech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupContainerView : UIView

+ (id)dialogWithController:(UIViewController*)controller size:(CGSize)size showTitle:(BOOL)showTitle;

- (id)initWithController:(UIViewController*)controller size:(CGSize)size showTitle:(BOOL)showTitle;

- (void)closePopupWithCompletion:(void(^)(void))completion;

@end
