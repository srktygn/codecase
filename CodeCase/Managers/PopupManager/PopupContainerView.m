//
//  PopupContainerView.m
//  Halkbank
//
//  Created by Serkut YEGİN on 16.09.2015.
//  Copyright © 2015 tmobtech. All rights reserved.
//

#import "PopupContainerView.h"

#define HEIGHT_FOR_TITLE 50

@interface PopupContainerView ()

@property (nonatomic) CGSize size;
@property (nonatomic,strong) UIViewController *controller;
@property (nonatomic) BOOL showTitle;

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *viewPopupContainer;
@property (strong, nonatomic) IBOutlet UIView *viewBackground;
@property (strong, nonatomic) IBOutlet UIView *viewTitle;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constCenterY;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constHeightTitle;

@end

@implementation PopupContainerView

+ (id)dialogWithController:(UIViewController*)controller size:(CGSize)size showTitle:(BOOL)showTitle
{
    PopupContainerView *dialog = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:self options:nil].firstObject;
    
    if(dialog){
        dialog.showTitle = showTitle;
        dialog.size = size;
        dialog.controller = controller;
    }
    
    return dialog;
}

- (id)initWithController:(UIViewController*)controller size:(CGSize)size showTitle:(BOOL)showTitle
{
    self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil].firstObject;
    
    if(self){
        self.showTitle = showTitle;
        self.size = size;
        self.controller = controller;
    }
    
    return self;
}

-(void)setController:(UIViewController *)controller
{
    [self removeController];
    
    _controller = controller;
    
    [self.viewContainer addSubview:_controller.view];
    
    if (self.showTitle) {
        self.lblTitle.text = _controller.title;
        self.constHeightTitle.constant = HEIGHT_FOR_TITLE;
    } else {
        self.constHeightTitle.constant = 0;
    }
    
    [_controller.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    UIView *subView = _controller.view;
    [_controller.view.superview addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-0-[subView]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(subView)]];
    [_controller.view.superview addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-0-[subView]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(subView)]];
    
    _constHeight.constant = _size.height;
    _constWidth.constant = _size.width;
    _constCenterY.constant = -[UIApplication sharedApplication].keyWindow.rootViewController.view.frame.size.height/2 - _size.height/2;
    
    [_controller.view layoutIfNeeded];
    [_viewTitle layoutIfNeeded];
    [_viewContainer layoutIfNeeded];
    [_viewPopupContainer layoutIfNeeded];
    [self layoutIfNeeded];

    [self.viewBackground showWithAlphaAnimation:0.5 completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _constCenterY.constant = 0;
        [_viewPopupContainer animateConstraintsWithCompletion:nil];
    });
}

-(void)removeController
{
    if (_controller) {
        [_controller.view removeFromSuperview];
        _controller = nil;
    }
}

-(void)closePopup
{
    [self closePopupWithCompletion:nil];
}

-(void)closePopupWithCompletion:(void(^)(void))completion
{
    [self.viewBackground hideWithAlphaAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _constCenterY.constant = -[UIApplication sharedApplication].keyWindow.rootViewController.view.frame.size.height/2 - _size.height/2;
        [_viewPopupContainer animateConstraintsWithCompletion:^{
            [self removeController];
            [self removeFromSuperview];
            
            if (completion) {
                completion();
            }
        }];
    });
    
}

#pragma mark - IBACtions

- (IBAction)btnDoneTapped:(UIButton *)sender {
    [[PopupManager shared] closeDialog];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
