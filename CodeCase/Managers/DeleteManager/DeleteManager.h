//
//  DeleteManager.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeleteManager : NSObject

#pragma mark - Public Methods
+ (id)shared;

-(BOOL)isItemDeletedWithTrackId:(double)trackId;
-(void)itemDeleted:(CC_Results*)item;

@end
