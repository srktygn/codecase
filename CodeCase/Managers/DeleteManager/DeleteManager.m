//
//  DeleteManager.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "DeleteManager.h"

static id deleteManager = nil;

#define USER_DEFAULTS_KEY_DELETE @"deletedItems"

@interface DeleteManager ()

@property (nonatomic,strong) NSMutableDictionary *dictDeletedItems;

@end

@implementation DeleteManager

+(id)shared
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        deleteManager = [[super allocWithZone:NULL] init];
    });
    
    return deleteManager;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return [self shared];
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _dictDeletedItems = [NSMutableDictionary new];
        [self readItemsFromUserDefaults];
    }
    
    return self;
}

-(BOOL)isItemDeletedWithTrackId:(double)trackId
{
    return [_dictDeletedItems objectForKey:[NSString stringWithFormat:@"%f",trackId]];
}

-(void)itemDeleted:(CC_Results*)item
{
    [_dictDeletedItems setObject:@"" forKey:[NSString stringWithFormat:@"%f",item.trackId]];
    [self writeItemsToUserDefaults];
}

-(void)readItemsFromUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictStoredItems = [defaults objectForKey:USER_DEFAULTS_KEY_DELETE];
    
    if (dictStoredItems) {
        _dictDeletedItems = [NSMutableDictionary dictionaryWithDictionary:dictStoredItems];
    }
}

-(void)writeItemsToUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_dictDeletedItems forKey:USER_DEFAULTS_KEY_DELETE];
    [defaults synchronize];
}

@end
