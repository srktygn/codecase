//
//  Managers.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#ifndef Managers_h
#define Managers_h

#import "PopupManager.h"
#import "VisitManager.h"
#import "DeleteManager.h"

#endif /* Managers_h */
