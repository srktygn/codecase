//
//  MediaTableCell.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaTableCell : UITableViewCell

-(void)setupUIWithTitle:(NSString*)title;
-(void)setupUIWithTitle:(NSString*)title selected:(BOOL)selected;

@property (nonatomic) BOOL isSelected;

@end
