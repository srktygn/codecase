//
//  MediaSelectionViewController.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "BaseViewController.h"
#import "SearchHelper.h"

@class MediaSelectionViewController;

@protocol MediaSelectionViewControllerDelegate <NSObject>

-(void)mediaSelectionVC:(MediaSelectionViewController*)mediaSelectionVC didChangeSelectionWithType:(SearchViewType)newType;

@end

@interface MediaSelectionViewController : BaseViewController

#pragma mark - Delegate
@property (nonatomic,weak) id<MediaSelectionViewControllerDelegate> delegate;

#pragma mark - Public Props
@property (nonatomic) SearchViewType selectedSearchType;

@end
