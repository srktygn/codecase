//
//  MediaSelectionViewController.m
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "MediaSelectionViewController.h"
#import "MediaTableCell.h"

@interface MediaSelectionViewController () <UITableViewDelegate,UITableViewDataSource>

#pragma mark - IBOutlets
@property (strong, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Props
@property (nonatomic,strong) NSArray *arrMedias;
@property (nonatomic) SearchViewType cachedSelectedSearchType;

@end

@implementation MediaSelectionViewController

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrMedias = @[@"All",@"Movie",@"Podcast",@"Music"];
    _cachedSelectedSearchType = _selectedSearchType;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Super Methods

-(NSString *)title
{
    return @"Please choose a media type.";
}

#pragma mark - IBActions

- (IBAction)btnDone:(UIButton *)sender {
    
    [[PopupManager shared] closeDialog];
    
    if (_selectedSearchType != _cachedSelectedSearchType) {
        
        if ([self.delegate respondsToSelector:@selector(mediaSelectionVC:didChangeSelectionWithType:)]) {
            [self.delegate mediaSelectionVC:self didChangeSelectionWithType:_selectedSearchType];
        }
    }
}

#pragma mark - UITableView Datasources & Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrMedias.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MediaTableCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MediaTableCell class])];
    
    if(cell==nil)
        cell = [[MediaTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([MediaTableCell class])];
    
    [cell setupUIWithTitle:_arrMedias[indexPath.row] selected:_selectedSearchType == indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != _selectedSearchType) {
        MediaTableCell *currentlySelectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedSearchType inSection:0]];
        currentlySelectedCell.isSelected = NO;
        MediaTableCell *nextSelectedCell = [tableView cellForRowAtIndexPath:indexPath];
        nextSelectedCell.isSelected = YES;
        _selectedSearchType = indexPath.row;
    }
    
}


@end
