//
//  MediaTableCell.m
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "MediaTableCell.h"

@interface MediaTableCell ()

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation MediaTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.selected = _isSelected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setupUIWithTitle:(NSString*)title
{
    _lblTitle.text = title;
}

-(void)setupUIWithTitle:(NSString*)title selected:(BOOL)selected
{
    _lblTitle.text = title;
    _isSelected = selected;
    self.selected = _isSelected;
}

-(void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    self.selected = _isSelected;
}

@end
