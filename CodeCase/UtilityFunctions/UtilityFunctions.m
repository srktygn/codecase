//
//  UtilityFunctions.m
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "UtilityFunctions.h"

#define TMP NSTemporaryDirectory()

@implementation UtilityFunctions

- (void)cacheImage:(NSString *)ImageURLString
     cacheFileName:(NSString*)cacheFileName
        completion:(void(^)(void))completion
{
    NSURL *ImageURL = [NSURL URLWithString:ImageURLString];
    
    // Generate a unique path to a resource representing the image you want
    NSString *filename = cacheFileName;
    NSString *uniquePath = [TMP stringByAppendingPathComponent:filename];
    
    // Check for file existence
    if(![[NSFileManager defaultManager] fileExistsAtPath:uniquePath])
    {
        // The file doesn't exist, we should get a copy of it
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:ImageURL];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (!error)
                                   {
                                       UIImage *image = [[UIImage alloc] initWithData:data];
                                       
                                       if([ImageURLString rangeOfString:@".png" options: NSCaseInsensitiveSearch].location != NSNotFound)
                                       {
                                           [UIImagePNGRepresentation(image) writeToFile:uniquePath atomically:YES];
                                       }
                                       else if(
                                               [ImageURLString rangeOfString:@".jpg" options: NSCaseInsensitiveSearch].location != NSNotFound ||
                                               [ImageURLString rangeOfString:@".jpeg" options: NSCaseInsensitiveSearch].location != NSNotFound
                                               )
                                       {
                                           [UIImageJPEGRepresentation(image, 100) writeToFile: uniquePath atomically: YES];
                                       }
                                       
                                       if (completion) {
                                           completion();
                                       }
                                       
                                   } else{
                                       
                                   }
                               }];
        
    }
}

- (void)getCachedImage:(NSString *)ImageURLString
         cacheFileName:(NSString*)cacheFileName
            completion:(void(^)(UIImage *cachedImage))completion
{
    NSString *filename = cacheFileName;
    NSString *uniquePath = [TMP stringByAppendingPathComponent: filename];
    
    __block UIImage *image;
    
    // Check for a cached version
    if([[NSFileManager defaultManager] fileExistsAtPath: uniquePath])
    {
        image = [UIImage imageWithContentsOfFile: uniquePath]; // this is the cached image
        if (completion) {
            completion(image);
        }
    }
    else
    {
        // get a new one
        [self cacheImage: ImageURLString cacheFileName:cacheFileName completion:^{
            image = [UIImage imageWithContentsOfFile: uniquePath];
            if (completion) {
                completion(image);
            }
        }];
        
    }
}

@end
