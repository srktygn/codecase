//
//  NSDate+Utilities.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

+(NSString*)dateStringWithDate:(NSDate*)date;
+(NSDate*)dateFromString:(NSString*)stringDate;

@end
