//
//  NSDate+Utilities.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

+(NSString*)dateStringWithDate:(NSDate*)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    
    
    return [dateFormat stringFromDate:date];
}

+(NSDate*)dateFromString:(NSString*)stringDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    return [formatter dateFromString:stringDate];
}

@end
