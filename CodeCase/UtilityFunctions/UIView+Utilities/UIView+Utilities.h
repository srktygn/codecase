//
//  UIView+Utilities.h
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ALPHA_ANIMATION_DURATION 0.5
#define CONSTRAINT_ANIMATION_DURATION 0.3

@interface UIView (Utilities)

#pragma mark - Frame

-(void)setX:(float)x;
-(void)setY:(float)y;
-(void)setW:(float)w;
-(void)setH:(float)h;
-(void)setX:(float)x y:(float)y;
-(void)setW:(float)w H:(float)h;
-(void)setY:(float)y H:(float)h;

#pragma mark - Alpha Animation
-(void)setAlpha:(CGFloat)alpha withAnimation:(BOOL)animation;
-(void)showWithAlphaAnimation;
-(void)showWithAlphaAnimation:(CGFloat)alpha;
-(void)showWithAlphaAnimation:(CGFloat)alpha completion:(void(^)(void))completion;
-(void)showWithAlphaAnimation:(CGFloat)alpha completion:(void(^)(void))completion duration:(NSTimeInterval)duration;
-(void)hideWithAlphaAnimation;
-(void)hideWithAlphaAnimationWithCompletion:(void(^)(void))completion;
-(void)hideWithAlphaAnimationWithCompletion:(void(^)(void))completion duration:(NSTimeInterval)duration;

#pragma mark - Frame Animation

-(void)animateConstraintsWithCompletion:(void(^)(void))completion;
-(void)animateConstraintsWithDuration:(NSTimeInterval)duration completion:(void(^)(void))completion;

#pragma mark - First Responder

- (BOOL)findAndResignFirstResponder;

@end
