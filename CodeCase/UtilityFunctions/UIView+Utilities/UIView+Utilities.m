//
//  UIView+Utilities.m
//  CodeCase
//
//  Created by Serkut Yegin on 18.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "UIView+Utilities.h"

@implementation UIView (Utilities)

#pragma mark - Frame
-(void)setX:(float)x
{
    self.frame = CGRectMake(x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}
-(void)setY:(float)y
{
    self.frame = CGRectMake(self.frame.origin.x, y, self.frame.size.width, self.frame.size.height);
}
-(void)setW:(float)w
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, w, self.frame.size.height);
}
-(void)setH:(float)h
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, h);
}
-(void)setX:(float)x y:(float)y
{
    self.frame = CGRectMake(x, y, self.frame.size.width, self.frame.size.height);
}
-(void)setW:(float)w H:(float)h
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, w, h);
}

-(void)setY:(float)y H:(float)h
{
    self.frame = CGRectMake(self.frame.origin.x, y, self.frame.size.width, h);
}

#pragma mark - Animation

-(void)setAlpha:(CGFloat)alpha withAnimation:(BOOL)animation {
    
    if (animation) {
        
        [UIView animateWithDuration:0.5f animations:^{
            
            self.alpha = alpha;
        }];
        
    } else {
        
        self.alpha = alpha;
    }
}

-(void)showWithAlphaAnimation {
    
    [self showWithAlphaAnimation:1];
}

-(void)showWithAlphaAnimation:(CGFloat)alpha {
    
    [self showWithAlphaAnimation:alpha completion:nil];
}

-(void)showWithAlphaAnimation:(CGFloat)alpha completion:(void(^)(void))completion{
    
    [self showWithAlphaAnimation:alpha completion:completion duration:ALPHA_ANIMATION_DURATION];
}

-(void)showWithAlphaAnimation:(CGFloat)alpha completion:(void(^)(void))completion duration:(NSTimeInterval)duration {
    
    self.hidden = NO;
    self.alpha = 0;
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = alpha;
    } completion:^(BOOL finished) {
        if (finished && completion) {
            completion();
        }
    }];
    
}

-(void)hideWithAlphaAnimation {
    
    [self hideWithAlphaAnimationWithCompletion:nil];
}

-(void)hideWithAlphaAnimationWithCompletion:(void(^)(void))completion {
    
    [self hideWithAlphaAnimationWithCompletion:completion duration:ALPHA_ANIMATION_DURATION];
}

-(void)hideWithAlphaAnimationWithCompletion:(void(^)(void))completion duration:(NSTimeInterval)duration{
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.hidden = YES;
        if (finished && completion) {
            completion();
        }
    }];
}

#pragma mark - Frame Animation

-(void)animateConstraintsWithCompletion:(void(^)(void))completion
{
    [self animateConstraintsWithDuration:CONSTRAINT_ANIMATION_DURATION completion:completion];
}
-(void)animateConstraintsWithDuration:(NSTimeInterval)duration completion:(void(^)(void))completion
{
    [UIView animateWithDuration:CONSTRAINT_ANIMATION_DURATION delay:0 usingSpringWithDamping:500 initialSpringVelocity:0 options:UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self layoutIfNeeded];
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished && completion) {
            completion();
        }
    }];
}

#pragma mark - First Responder

- (BOOL)findAndResignFirstResponder {
    if (self.isFirstResponder) {
        [self resignFirstResponder];
        return YES;
    }
    
    for (UIView *subView in self.subviews) {
        if ([subView findAndResignFirstResponder]) {
            return YES;
        }
    }
    return NO;
}

@end
