//
//  UIColor+Utilities.m
//  CodeCase
//
//  Created by Serkut Yegin on 20.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "UIColor+Utilities.h"

@implementation UIColor (Utilities)

+(UIColor *)appMainColor
{
    return RGB(255, 204, 102);
}

@end
