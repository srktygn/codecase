//
//  UIColor+Utilities.h
//  CodeCase
//
//  Created by Serkut Yegin on 20.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (Utilities)

+(UIColor*)appMainColor;

@end
