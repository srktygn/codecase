//
//  NSString+Utilities.h
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utilities)

- (BOOL)isEmpty;
- (BOOL)isAlphaNumerical;

@end
