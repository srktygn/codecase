//
//  NSString+Utilities.m
//  CodeCase
//
//  Created by Serkut Yegin on 19.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import "NSString+Utilities.h"

@implementation NSString (Utilities)

- (BOOL)isEmpty
{
    if(self == nil || self == (id)[NSNull null] || [[self stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""])
        return YES;
    return NO;
}

-(BOOL)isAlphaNumerical {
    
    BOOL returnValue = TRUE;
    
    NSCharacterSet *Set = [NSCharacterSet alphanumericCharacterSet];
    
    for (int i = 0; i < [self length]; i++) {
        returnValue = [Set characterIsMember:[self characterAtIndex:i]] && returnValue;
    }
    
    return returnValue;
}

@end
