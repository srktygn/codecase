//
//  UtilityFunctions.h
//  CodeCase
//
//  Created by Serkut Yegin on 17.05.2016.
//  Copyright © 2016 Serkut Yegin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UtilityFunctions : NSObject

- (void)cacheImage:(NSString *)ImageURLString
     cacheFileName:(NSString*)cacheFileName
        completion:(void(^)(void))completion;

- (void)getCachedImage:(NSString *)ImageURLString
         cacheFileName:(NSString*)cacheFileName
            completion:(void(^)(UIImage *cachedImage))completion;

@end
